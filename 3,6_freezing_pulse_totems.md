[3.6] Cheap, easy (AFK mode), Freezing Pulse + totem guide

# Gems

## 6-link (in order)

R Spell Totem  
B Freezing Pulse  
B Controlled Destruction  
G Cold Penetration  
B Increased Critical Strikes  
B Faster Casting

## 4-links

R Immortal Call (3, 0Q)  
R CWDT (1, 0Q)  
R Increased duration  
G Enfeeble (5)  

B Orb of Storms  
B Curse on Hit  
B Frostbite  
G Ice Golem (Empower if ice Golem is in ring)  

## 3-links

B Flame Dash  
B Arcane Surge (14, 0Q)  
B Faster Casting  

B Phase Run  
R Increase duration  
B Efficacy  

## 2-link

B Zealotry  
B Enlighten  

## Socket ring (optional)

B Vaal Discipline  
G Ice Golem

***

# Flasks (cheap version)

Set superior hallowed hybrid and diamond flasks to **ON** in your loot filter!

Bubbling Hallowed Hybrid Flask of Grounding  
Bubbling Hallowed Hybrid Flask of Staunching  
Chemist's diamond flask of curing (if above 70%, change to a resist + "curing" flask)  
The Wise Oak  
Kiara's Determination  

***

# Pantheon

## Major

Main: Lunaris  
Bosses: Arakaali  
Never: Brine King (Kiara's Determination > Brine King)  

## Minor
Main: Gruthkul  
Bosses: Shakari  
Delve: Tukohama  
Never: Yugul  

***

# Gear

This build hinges on two Frozen Trail jewels. Try to get higher projectile damage from the jewels.

## Up to mid tier maps

Shimmeron (<5c -- replace with Void Battery, then crafted Void Sceptre) http://poe.trade/search/akonomasehinih  
Rathpith (<15c) http://poe.trade/search/itamakiniwamot  
Tabula Rasa (<20c, min 8c -- upgrade to Belly of the Beast to hit >5,5k HP)  
Voidbringer/Maligaro's Virtuosity (<2c) or Winds of Change (<15c, don't buy for Freezing Pulse)  
[HELM] (FP/FB enchant)  
+Dex and lots of it

Ammy: https://poe.trade/search/oyonahohitasar  
Ring: https://poe.trade/search/kokiurituwasit  
https://poe.trade/search/osimamoominoar  

Belt: https://poe.trade/search/nogagonazuogam + https://poe.trade/search/gonahahomorioy  
Abyss jewels (2 or 4): https://poe.trade/search/onarehakeyatom  
For abyss jewel, life + ES, all resist  

Helm (need extra suffix slot for dex + quality craft): https://poe.trade/search/asamomerenyumk

### Ring craft
https://poe.trade/search/unometaihinata or https://poe.trade/search/iyahumihamitok  

### Damage stats priority

1) Spell damage  
2) Cold damage to spells  
3) Crit  

## Higher tier

Flask: Atziri's Acuity  
Body: Belly of the Beast  
Boots: 2-abyss Bubonic Trail  
Belt: Darkness Enthroned  
Gloves: Voidbringer  
Eye jewels: 50 HP + ES, 10 all elemental resist

# Skill tree

## Passive skill tree

Blahs

## Ascendancy

The Hierophant

1) Pursuit of Faith  
2) Ritual of Awakening  
3) Conviction of Power  
4) Divine Guidance  

***

# POB

??
